﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewBarritas : MonoBehaviour {

	//bars
	public Image b1, b2, b3, b4, b5, b6, b7, b8, b9;
	public Image[] myBarritas;
	//temporal HP variable
	public float hitpoints;
	//current hp para manejo interno
	public float currentHP;
	//asociate height with the hp
	public int height;
	//each separate height
	public float h1, h2, h3, h4, h5, h6, h7, h8, h9;
	public float hh1, hh2, hh3, hh4, hh5, hh6, hh7, hh8, hh9;
	//minimun value to have
	public float zero = 0.0f;
	public float val = .2f;

	// Use this for initialization
	void Start () 
	{
		//starting health
		hitpoints = 120;
		//set starting
		JustFillItBaby ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		//rotation of the rotholder
		transform.Rotate(0, 20 * Time.deltaTime, 0);
		//updating hitpoints
		hitpoints = currentHP;

		//checking heights
		h1 = b1.fillAmount;
		h2 = b2.fillAmount;
		h3 = b3.fillAmount;
		h4 = b4.fillAmount;
		h5 = b5.fillAmount;
		h6 = b6.fillAmount;
		h7 = b7.fillAmount;
		h8 = b8.fillAmount;
		h9 = b9.fillAmount;
		//works the height through the amount of hp method
		HPChecker ();
		//changes the colour depending of hp amount method
		CheckItBaby ();


	
	}

	void JustFillItBaby ()
	{
		/*
		foreach (Image i in myBarritas)
		{
			i.fillAmount += 10.0f * Time.deltaTime;
		}*/

		b1.fillAmount += 1f * Time.deltaTime;
		b2.fillAmount += 1f * Time.deltaTime;
		b3.fillAmount += 1f * Time.deltaTime;
		b4.fillAmount += 1f * Time.deltaTime;
		b5.fillAmount += 1f * Time.deltaTime;
		b6.fillAmount += 1f * Time.deltaTime;
		b7.fillAmount += 1f * Time.deltaTime;
		b8.fillAmount += 1f * Time.deltaTime;
		b9.fillAmount += 1f * Time.deltaTime;

		
	}

	void JustEmptyBaby ()
	{
		/*
		foreach (Image i in myBarritas)
		{
			i.fillAmount -= 10.0f * Time.deltaTime;
		}*/

		b1.fillAmount -= 1f/val * Time.deltaTime;
		b2.fillAmount -= 1f/val * Time.deltaTime;
		b3.fillAmount -= 1f/val * Time.deltaTime;
		b4.fillAmount -= 1f/val * Time.deltaTime;
		b5.fillAmount -= 1f/val * Time.deltaTime;
		b6.fillAmount -= 1f/val * Time.deltaTime;
		b7.fillAmount -= 1f/val * Time.deltaTime;
		b8.fillAmount -= 1f/val * Time.deltaTime;
		b9.fillAmount -= 1f/val * Time.deltaTime;


	}
	//works the fill amount depending in hp of char and /100 to get the height 
	void HPChecker ()
	{
		b1.fillAmount = currentHP / 100f;
		b2.fillAmount = currentHP / 100f;
		b3.fillAmount = currentHP / 100f;
		b4.fillAmount = currentHP / 100f;
		b5.fillAmount = currentHP / 100f;
		b6.fillAmount = currentHP / 100f;
		b7.fillAmount = currentHP / 100f;
		b8.fillAmount = currentHP / 100f;
		b9.fillAmount = currentHP / 100f;

	}

	void FillToHP ()
	{
		hh1 = h1 * 100;
		hh2 = h2 * 100;
		hh3 = h3 * 100;
		hh4 = h4 * 100;
		hh5 = h5 * 100;
		hh6 = h6 * 100;
		hh7 = h7 * 100;
		hh8 = h8 * 100;
		hh9 = h9 * 100;
	}

	//filling with colours the bars
	void JustColorItBaby (Color color)
	{
		foreach (Image i in myBarritas)
		{
			i.color = color;
		}

	}

	//checking the amount of HP to colour the bars
	void CheckItBaby ()
	{
		JustFillItBaby ();

		FillToHP ();

		if (currentHP >= 70 && currentHP <= 120)
		{
			JustColorItBaby (color: Color.green);

			if (hh1 == 120 && hh2 == 120 && hh3 == 120 && hh4 == 120 && hh5 == 120 &&
			    hh6 == 120 && hh7 == 120 && hh8 == 120 && hh9 == 120)
			{
				print ("$$$$$$$$$$$$$$$$$$$$$");
				JustEmptyBaby ();
			}



			if (hh1 == currentHP && hh2 == currentHP && hh3 == currentHP && hh4 == currentHP && hh5 == currentHP &&
			    hh6 == currentHP && hh7 == currentHP && hh8 == currentHP && hh9 == currentHP)
			{
				print ("$$$$$$$$$$$$$$$$$$$$$");
				JustEmptyBaby ();
			}


			if (hh1 == zero && hh2 == zero && hh3 == zero && hh4 == zero && hh5 == zero &&
			    hh6 == zero && hh7 == zero && hh8 == zero && hh9 == zero)
			{
				print ("&7&&777777777777777777777$");
				JustFillItBaby ();
			}

		}

		if (hitpoints >= 40 && hitpoints <= 69)
		{
			JustColorItBaby (color: Color.yellow);
			
			
	
		}

		if (hitpoints >= 0 && hitpoints <= 39)
		{
			JustColorItBaby (color: Color.red);
			
			
			
		}

	}

}
